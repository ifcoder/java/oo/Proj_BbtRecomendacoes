package com.oo.projbbtrecomendacoes;

import com.oo.projbbtrecomendacoes.entity.Autor;
import com.oo.projbbtrecomendacoes.classes.Livro;
import com.oo.projbbtrecomendacoes.entity.Usuario;
import com.oo.projbbtrecomendacoes.servicos.Biblioteca;
import java.util.List;

/**
 *
 * @author jose
 */
public class ProjBbtRecomendacoes {

    public static void main(String[] args) {
        Biblioteca biblioteca = new Biblioteca();
        
        Usuario usuario = new Usuario("João");
        Usuario usuario2 = new Usuario("Maria");

        usuario.adicionarGeneroFavorito("Drama");
        usuario2.adicionarGeneroFavorito("Fantasia");

        // Criação de autores
        Autor tolkien = new Autor("J.R.R. Tolkien", "Cidade01");
        Autor orwell = new Autor("George Orwell", "Cidade02");
        Autor rowling = new Autor("J.K. Rowling", "Cidade03");
        Autor huxley = new Autor("Aldous Huxley", "Cidade04");

        // Adicionando livros à biblioteca inicialmente sem autores
        Livro oHobbit = new Livro("O Hobbit", 1937);
        Livro livro1984 = new Livro("1984", 1949);
        Livro harryPotter = new Livro("Harry Potter e a Pedra Filosofal", 1997);
        Livro admiravelMundoNovo = new Livro("Admirável Mundo Novo", 1932);

        // Adicionando autores aos livros
        oHobbit.addAutor(tolkien);
        livro1984.addAutor(orwell);
        harryPotter.addAutor(rowling);
        admiravelMundoNovo.addAutor(huxley);

        // Adicionando genero nos livros
        oHobbit.adicionarGenero("Fantasia");
        livro1984.adicionarGenero("Ficção Científica");
        livro1984.adicionarGenero("Drama");
        harryPotter.adicionarGenero("Fantasia");
        admiravelMundoNovo.adicionarGenero("Ficção Científica");

        // Agora adicionando os livros à biblioteca
        biblioteca.adicionarLivro(oHobbit);
        biblioteca.adicionarLivro(livro1984);
        biblioteca.adicionarLivro(harryPotter);
        biblioteca.adicionarLivro(admiravelMundoNovo);
        

        // adicionando usuários à biblioteca
        biblioteca.adicionarUsuario(usuario);
        biblioteca.adicionarUsuario(usuario2);

        // O usuário lê e avalia um livro
       
        biblioteca.avaliarLivro("O Hobbit", 4.0, usuario);
        biblioteca.avaliarLivro("Admirável Mundo Novo", 3.0, usuario);
        biblioteca.avaliarLivro("Harry Potter e a Pedra Filosofal", 5.0, usuario2);
        biblioteca.avaliarLivro("1984", 4.5, usuario2);
        

        // Solicitando recomendações de livros para o usuário
        List<Livro> recomendacoesJoao = biblioteca.recomendarLivrosParaUsuario(usuario.getNome());
        System.out.println("Livros recomendados para João:");
        for (Livro l : recomendacoesJoao) {
            l.imprimirDetalhes();
        }

        List<Livro> recomendacoesMaria = biblioteca.recomendarLivrosParaUsuario(usuario2.getNome());
        System.out.println("\nLivros recomendados para Maria:");
        for (Livro l : recomendacoesMaria) {
            l.imprimirDetalhes();
        }
        
    }

}
