# Proj_BBTRecomendacoes

### Exercicio 14 (Sistema de Recomendação de Livros na BBT) ⭐⭐⭐⭐

Este exercício expande um sistema de biblioteca para incluir um sistema de recomendação de livros. A biblioteca gerencia uma coleção de livros e os usuários que interagem com esses livros, oferecendo recomendações personalizadas com base nos gêneros favoritos dos usuários e nas avaliações dos livros.
![Imagem](./src/main/resources/images/Miniatura.png)

### Descrição:

1. **Classe `Autor`:**
    - Atributos: `nome`, `cidadeNatal`.
    - Métodos: Construtores, getters e setters conforme necessário.
2. **Classe `Livro`:**
    - Atributos: `titulo`, `autores` (Lista de `Autor`), `anoPublicacao`, `generos` (Lista de strings), `avaliacao` (do tipo `Avaliacao`).
    - Métodos: Construtores, getters e setters, método para imprimir detalhes, adicionar uma avaliação, adicionar e remover autor(es).
3. **Classe `Avaliacao`:**
    - Atributos: `notaAcumulada`, `numeroDeAvaliacoes`.
    - Métodos: Adicionar uma nova avaliação (com validação de nota entre 0 e 5), calcular a média das avaliações.
4. **Classe `Usuario`:**
    - Atributos: `nome`, `generosFavoritos` (Lista de strings), `livrosLidos` (Lista de `Livro`).
    - Métodos: Adicionar/remover gêneros favoritos, adicionar livro lido com avaliação.
5. **Classe `Biblioteca`:**
    - Atributos: `livros` (Lista de todos os livros disponíveis), `usuarios` (Lista de usuários).
    - Métodos: Adicionar/remover livros e usuários, buscar livros por autor/gênero, listar todos os livros, avaliar um livro (atualizando o rating através da classe `Avaliacao`), recomendar livros para um usuário específico com base em seus gêneros favoritos e avaliações de livros acima de 3.5.

### Funcionalidades Importantes:

- O sistema de recomendações é responsabilidade da classe `Biblioteca`, que analisa os gêneros favoritos de um usuário e os livros não lidos por ele, recomendando livros com avaliações médias acima de 3.5.
- Após ler um livro, um usuário deve ser capaz de avaliá-lo com uma nota de 0 a 5. Esta nota é processada pela `Biblioteca`, garantindo que todas as avaliações reflitam no estado global dos livros.
- Implemente um menu interativo que permita aos usuários acessar cada funcionalidade do sistema.

### Exemplo de Uso:

```java
public static void main(String[] args) {
        Biblioteca biblioteca = new Biblioteca();
        Usuario usuario = new Usuario("João");
        Usuario usuario2 = new Usuario("Maria");
        
        usuario.adicionarGeneroFavorito("Drama");

        // Criação de autores
        Autor tolkien = new Autor("J.R.R. Tolkien", "Cidade01");
        Autor orwell = new Autor("George Orwell", "Cidade02");

        // Adicionando livros à biblioteca inicialmente sem autores
        Livro oHobbit = new Livro("O Hobbit", 1937);
        Livro livro1984 = new Livro("1984", 1949);

        // Adicionando autores aos livros
        oHobbit.adicionarAutor(tolkien);
        livro1984.adicionarAutor(orwell);

        // Adicionando genero nos livros
        oHobbit.getGeneros().add("Fantasia"); //Assim estamos expondo demais nosso atributo List de genero
        livro1984.adicionarGenero("Ficção Científica"); //desta forma é melhor (Encapsulamento, Manutencao e flexibilidade)
        livro1984.adicionarGenero("Drama");

        // Agora adicionando os livros à biblioteca
        biblioteca.adicionarLivro(oHobbit);
        biblioteca.adicionarLivro(livro1984);

        // adicionando usuários a biblioteca
        biblioteca.adicionarUsuario(usuario);
        biblioteca.adicionarUsuario(usuario2);        
        
        // O usuário lê e avalia um livro
        biblioteca.avaliarLivro("1984", 4.5, usuario2);
        biblioteca.avaliarLivro("O Hobbit", 4.0, usuario);
        

        // Solicitando recomendações de livros para o usuário
        List<Livro> recomendacoes = biblioteca.recomendarLivrosParaUsuario(usuario.getNome());
        System.out.println("Livros recomendados para você:");
        for(Livro l : recomendacoes)
            l.imprimirDetalhes();
    }
```

Este exercício desafia você a aplicar conceitos de Programação Orientada a Objetos, como encapsulamento e responsabilidade única, ao mesmo tempo em que oferece um sistema funcional e útil de gerenciamento de biblioteca com recomendações personalizadas para os usuários.
